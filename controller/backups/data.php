<?php
/*
 * Simple JSON generation from static array
 * Author: Ravi Tamada
 * web: www.androidhive.info
 *
 * This is not a RAW json format
 * A JSON encoder will parse these arrays into a JSON format
 */

$categories_list = array(
    1 => array(
        "id" => 1,
        "category" => "127 Hours",
        "items" => array(
            array(
                "id" => 1,
                "name" => "Baju",
                "price" => "5:52"
            ),
            array(
                "id" => 2,
                
                "name" => "The Canyon",
                "price" => "3:01"
            ),
            array(
                "id" => 3,
                "name" => "Liberation Begins",
                "price" => "2:14"
            ),
            array(
                "id" => 4,
                "name" => "Touch of the Sun",
                "price" => "4:39"
            ),
            array(
                "id" => 5,
                "name" => "Lovely Day",
                "price" => "4:16"
            ),
            array(
                "id" => 6,
                "name" => "Ca Plane Pour Moi",
                "price" => "3:00"
            ),
            array(
                "id" => 7,
                "name" => "Liberation In A Dream",
                "price" => "4:06"
            ),
            array(
                "id" => 8,
                "name" => "If You Love Me (Really Love Me)",
                "price" => "3:27"
            ),
            array(
                "id" => 9,
                "name" => "Acid Darbari",
                "price" => "4:21"
            ),
            array(
                "id" => 10,
                "name" => "R.I.P.",
                "price" => "5:11"
            ),
            array(
                "id" => 11,
                "name" => "Festival",
                "price" => "9:26"
            ),
            array(
                "id" => 12,
                "name" => "If I Rise",
                "price" => "4:38"
            ),
            array(
                "id" => 13,
                "name" => "Liberation",
                "price" => "3:11"
            ),
            array(
                "id" => 14,
                "name" => "Nocturne No. 2 in E flat",
                "price" => "4:01"
            )
        )
    ),
    2 => array(
        "id" => 2,
        "category" => "Adele 21",
        "items" => array(
            array(
                "id" => 1, 
                "name" => "Rolling In The Deep", 
                "price" => "03:48"
            ),
            array(
                "id" => 2,
                "name" => "Rumour Has It",
                "price" => "03:43"
            ),
            array(
                "id" => 3,
                "name" => "Turning Tables",
                "price" => "04:10"
            ),
            array(
                "id" => 4,
                "name" => "Don’t You Remember",
                "price" => "04:03"
            ),
            array(
                "id" => 5,
                "name" => "Set Fire To The Rain",
                "price" => "04:02"
            ),
            array(
                "id" => 6,
                "name" => "He Won’t Go",
                "price" => "04:38"
            ),
            array(
                "id" => 7,
                "name" => "Take It All",
                "price" => "03:48"
            ),
            array(
                "id" => 8,
                "name" => "I’ll Be Waiting",
                "price" => "04:01"
            ),
            array(
                "id" => 9,
                "name" => "One And Only",
                "price" => "05:48"
            ),
            array(
                "id" => 10,
                "name" => "Lovesong",
                "price" => "05:16"
            ),
            array(
                "id" => 11,
                "name" => "Someone Like You",
                "price" => "04:45"
            )
        )
    ),
    3 => array(
        "id" => 3,
        "category" => "Lana Del Rey - Born to Die",
        "items" => array(
            array(
                "id" => 1,
                "name" => "Born to Die",
                "price" => "4:46"
            ),
            array(
                "id" => 2,
                "name" => "Off to the Races",
                "price" => "5:00"
            ),
            array(
                "id" => 3,
                "name" => "Blue Jeans",
                "price" => "3:29"
            ),
            array(
                "id" => 4,
                "name" => "Video Games",
                "price" => "4:42"
            ),
            array(
                "id" => 5,
                "name" => "Diet Mountain Dew",
                "price" => "3:43"
            ),
            array(
                "id" => 6,
                "name" => "National Anthem",
                "price" => "3:51"
            ),
            array(
                "id" => 7,
                "name" => "Dark Paradise",
                "price" => "4:03"
            ),
            array(
                "id" => 8,
                "name" => "Radio",
                "price" => "3:35"
            ),
            array(
                "id" => 9,
                "name" => "Carmen",
                "price" => "4:09"
            ),
            array(
                "id" => 10,
                "name" => "Million Dollar Man",
                "price" => "3:50"
            ),
            array(
                "id" => 11,
                "name" => "Summertime Sadness",
                "price" => "4:25"
            ),
            array(
                "id" => 12,
                "name" => "This Is What Makes Us Girls",
                "price" => "4:00"
            )
        )
    ),
    4 => array(
        "id" => 4,
        "category" => "Once",
        "items" => array(
            array(
                "id" => 1,
                "name" => "Falling Slowly",
                "price" => "4:05"
            ),
            array(
                "id" => 2,
                "name" => "If You Want Me",
                "price" => "3:47"
            ),
            array(
                "id" => 3,
                "name" => "Broken Hearted Hoover Fixer Sucker Guy",
                "price" => "0:52"
            ),
            array(
                "id" => 4,
                "name" => "When Your Mind's Made Up",
                "price" => "3:42"
            ),
            array(
                "id" => 5,
                "name" => "Lies",
                "price" => "3:58"
            ),
            array(
                "id" => 6,
                "name" => "Gold",
                "price" => "3:59"
            ),
            array(
                "id" => 7,
                "name" => "The Hill",
                "price" => "4:34"
            ),
            array(
                "id" => 8,
                "name" => "Fallen From The Sky",
                "price" => "3:24"
            ),
            array(
                "id" => 9,
                "name" => "Leave",
                "price" => "2:46"
            ),
            array(
                "id" => 10,
                "name" => "Trying To Pull Myself Away",
                "price" => "3:35"
            ),
            array(
                "id" => 11,
                "name" => "All The Way Down",
                "price" => "2:38"
            ),
            array(
                "id" => 12,
                "name" => "Once",
                "price" => "3:37"
            ),
            array(
                "id" => 13,
                "name" => "Say It To Me Now",
                "price" => "2:36"
            )
        )
    ),
    5 => array(
        "id" => 5,
        "category" => "Away We Go",
        "items" => array(
            array(
                "id" => 1,
                "name" => "All My Days",
                "price" => "4:56"
            ),
            array(
                "id" => 2,
                "name" => "Blue Mind",
                "price" => "5:42"
            ),
            array(
                "id" => 3,
                "name" => "What Is Life",
                "price" => "4:24"
            ),
            array(
                "id" => 4,
                "name" => "Song For You",
                "price" => "4:37"
            ),
            array(
                "id" => 5,
                "name" => "Golden Brown",
                "price" => "3:31"
            ),
            array(
                "id" => 6,
                "name" => "Towards The Sun",
                "price" => "4:40"
            ),
            array(
                "id" => 7,
                "name" => "Meet Me In The Morning",
                "price" => "4:22"
            ),
            array(
                "id" => 8,
                "name" => "Breathe",
                "price" => "4:16"
            ),
            array(
                "id" => 9,
                "name" => "Wait",
                "price" => "5:55"
            ),
            array(
                "id" => 10,
                "name" => "The Ragged Sea",
                "price" => "3:20"
            ),
            array(
                "id" => 11,
                "name" => "Oh! Sweet Nuthin'",
                "price" => "7:29"
            ),
            array(
                "id" => 12,
                "name" => "Orange Sky",
                "price" => "6:09"
            ),
            array(
                "id" => 13,
                "name" => "Crinan Wood",
                "price" => "5:45"
            )
        )
    ),
    6 => array(
        "id" => 6,
        "category" => "Eminem Curtain Call",
        "items" => array(
            array(
                "id" => 1,
                "name" => "My Name Is",
                "price" => "4:28"
            ),
            array(
                "id" => 2,
                "name" => "The Way I Am",
                "price" => "4:51"
            ),
            array(
                "id" => 3,
                "name" => "Lose Yourself",
                "price" => "5:21"
            ),
            array(
                "id" => 4,
                "name" => "Shake That",
                "price" => "4:34"
            ),
            array(
                "id" => 5,
                "name" => "Sing For The Moment",
                "price" => "5:40"
            ),
            array(
                "id" => 6,
                "name" => "Without Me",
                "price" => "4:47"
            ),
            array(
                "id" => 7,
                "name" => "Like Toy Soldiers",
                "price" => "4:51"
            ),
            array(
                "id" => 8,
                "name" => "The Real Slim Shady",
                "price" => "4:44"
            ),
            array(
                "id" => 9,
                "name" => "Mockingbird",
                "price" => "4:11"
            ),
            array(
                "id" => 10,
                "name" => "Guilty Conscience",
                "price" => "3:19"
            ),
            array(
                "id" => 11,
                "name" => "Cleanin' Out My Closet",
                "price" => "4:58"
            ),
            array(
                "id" => 12,
                "name" => "Just Lose It",
                "price" => "4:08"
            ),
            array(
                "id" => 13,
                "name" => "When I'm Gone",
                "price" => "4:40"
            ),
            array(
                "id" => 14,
                "name" => "Stan",
                "price" => "6:21"
            )
        )
    ),
    7 => array(
        "id" => 7,
        "category" => "Bad Meets Evil Eminem",
        "items" => array(
            array(
                "id" => 1,
                "name" => "Lighters",
                "price" => "5:21"
            ),
            array(
                "id" => 2,
                "name" => "Fast Lane",
                "price" => "3:19"
            ),
            array(
                "id" => 3,
                "name" => "Above the Law",
                "price" => "6:21"
            ),
            array(
                "id" => 4,
                "name" => "Welcome 2 Hell",
                "price" => "4:34"
            ),
            array(
                "id" => 5,
                "name" => "Take from Me",
                "price" => "4:51"
            ),
            array(
                "id" => 6,
                "name" => "The Reunion",
                "price" => "4:44"
            ),
            array(
                "id" => 7,
                "name" => "A Kiss",
                "price" => "3:19"
            ),
            array(
                "id" => 8,
                "name" => "Echo",
                "price" => "6:21"
            ),
            array(
                "id" => 9,
                "name" => "Lighters",
                "price" => "4:40"
            ),
            array(
                "id" => 10,
                "name" => "Living Proof",
                "price" => "3:49"
            ),
            array(
                "id" => 11,
                "name" => "Loud Noises",
                "price" => "3:19"
            )
        )
    ),
    8 => array(
        "id" => 8,
        "category" => "Safe Trip Home",
        "items" => array(
            array(
                "id" => 1,
                "name" => "Don't Believe in Love",
                "price" => "3:52"
            ),
            array(
                "id" => 2,
                "name" => "Quiet Times",
                "price" => "3:16"
            ),
            array(
                "id" => 3,
                "name" => "Never Want to Say It's Love",
                "price" => "3:34"
            ),
            array(
                "id" => 4,
                "name" => "Grafton Street",
                "price" => "5:56"
            ),
            array(
                "id" => 5,
                "name" => "It Comes And It Goes",
                "price" => "3:26"
            ),
            array(
                "id" => 6,
                "name" => "Look No Further",
                "price" => "3:15"
            ),
            array(
                "id" => 7,
                "name" => "Us 2 Little Gods",
                "price" => "4:48"
            ),
            array(
                "id" => 8,
                "name" => "The Day Before The Day",
                "price" => "4:12"
            ),
            array(
                "id" => 9,
                "name" => "Let's Do the Things We Normally Do",
                "price" => "4:08"
            ),
            array(
                "id" => 10,
                "name" => "Burnin Love",
                "price" => "4:10"
            ),
            array(
                "id" => 11,
                "name" => "Northern Skies",
                "price" => "8:53"
            )
        )
    ),
    9 => array(
        "id" => 9,
        "category" => "No Angel",
        "items" => array(
            array(
                "id" => 1,
                "name" => "Here With Me",
                "price" => "4:05"
            ),
            array(
                "id" => 2,
                "name" => "Hunter",
                "price" => "3:55"
            ),
            array(
                "id" => 3,
                "name" => "Don't Think Of Me",
                "price" => "4:32"
            ),
            array(
                "id" => 4,
                "name" => "My Lover's Gone",
                "price" => "4:27"
            ),
            array(
                "id" => 5,
                "name" => "All You Want",
                "price" => "3:53"
            ),
            array(
                "id" => 6,
                "name" => "Thank You",
                "price" => "3:36"
            ),
            array(
                "id" => 7,
                "name" => "Honestly Ok",
                "price" => "4:37"
            ),
            array(
                "id" => 8,
                "name" => "Slide",
                "price" => "4:50"
            ),
            array(
                "id" => 9,
                "name" => "Isobel",
                "price" => "3:55"
            ),
            array(
                "id" => 10,
                "name" => "I'm No Angel",
                "price" => "3:55"
            ),
            array(
                "id" => 11,
                "name" => "My Life",
                "price" => "2:59"
            ),
            array(
                "id" => 12,
                "name" => "Take My Hand",
                "price" => "6:50"
            )
        )
    )
);

echo json_encode($categories_list);
?>