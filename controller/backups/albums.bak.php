<?php

/*
 *	This is first level View
 *	Echoes list of albums
 *	Data is taken from data.php
 *
 *	example of output: http://api.androidhive.info/songs/albums.php
 */
include_once './data.php';
$albums = array();

// looping through each album
foreach ($album_tracks as $album) {
    $tmp = array();
    // returns id integer
    $tmp["id"] = $album["id"];
    // returns Album string
    $tmp["name"] = $album["album"];
    // returns no of item in Songs array
    $tmp["songs_count"] = count($album["songs"]);

    // push album
    array_push($albums, $tmp);
}

// printing json
// it doesn't RETURN $albums because in JSON Parser,
// 		it listens to it.
echo json_encode($albums);
?>
