<?php
/*
* Section 1: Connect db
* Section 2: retrieve image
* Section 3: get image filename
* Section 4: store/update in db
* Section 5: retrieve in android via img lazyload
*/
	
	/* 
	* Section 1:
	* Connect db */
	include_once "../include/db_connect.php";
	$db = new DB_Connect();
	$db->connect();

	/* 
	* Section 2:
	* Retrieve image from device via HTTP Request */
	if ($_POST) {
	    define('UPLOAD_DIR', 'uploads/');
	    $img = $_POST['image'];
	    $img = str_replace('data:image/jpeg;base64,', '', $img);
	    $img = str_replace(' ', '+', $img);
	    $data = base64_decode($img);

		/*
		* Section 3:
		* Get image filename */
		$username='testuser';
	    $file = UPLOAD_DIR . $username . "_" . uniqid() . '.jpg';
	    $success = file_put_contents($file, $data);
	    print $success ? $file : 'Unable to save the file.';


		/*
		* Section 4
		* Store or update directory and filename in db
		*/
		mysql_query("UPDATE users SET profile_photo_uri = '".$file."' WHERE name = 'testuser'");


		/*
		* Section 5
		* Retrieve in device via lazyloading
		*/
	}


?>

		<h1>Retrieve</h1>
