<?php

/*
 *	This is first level View
 *	Echoes list of albums
 *	Data is taken from data.php
 *
 *	example of output: http://api.androidhive.info/songs/albums.php
 */
include_once './data.php';
$categories = array();

// looping through each album
foreach ($categories_list as $category) {
    $tmp = array();
    // returns id integer
    $tmp["id"] = $category["id"];
    // returns Album string
    $tmp["category"] = $category["category"];
    // returns no of item in Songs array
    $tmp["categories_count"] = count($category["items"]);

    // push album
    array_push($categories, $tmp);
}

// printing json
// it doesn't RETURN $albums because in JSON Parser,
// 		it listens to it.
echo json_encode($categories);
?>
