# WebShopper - Server Side Scripts

#### Revision Changes
1. Rev 2.7
	+ Move uploaded file to another directory and rename it
	+ Refactored variables to sync with webshopper app
2. Rev 2.8
	+ Moved to bitbucket for private repo
3. Rev 3.0 (22/10/2013)
	+ Modified `data.php` to retrieve data from mysql db and storing it in array by following original format
	+ Fixed `items` id to reset according to items
4. Rev 3.4 (25/10/2013)
	+ Added Full Name field in `RegisterActivity`
5. Rev 3.5 (1/11/2013)
	+ Create new directory as new user registers
	+ Redirected uploaded item according to users directories
6. Rev 3.6 (2/12/2013)
    + Added Comments function
7. Rev 3.7 (4/12/2013)
	+ Added `user_item.php` to pull individual user items
8. Rev 3.8 (5/12/2013)
	+ Fixed directory issues
	+ Fixed Category listing data issues

---
Managed by Aiman Baharum (c) 2013